// TortoiseGit - a Windows shell extension for easy version control

// Copyright (C) 2008-2017 - TortoiseGit
// Copyright (C) 2012 - TortoiseSVN

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
#include "stdafx.h"
#include "ShellUpdater.h"

#include "LFSCommand.h"

#include "LocksDlg.h"

#include "MessageBox.h"

bool LFSCommand::Execute()
{
	CString format;
	BOOL lock = parser.HasKey(L"lock");
	BOOL unlock = parser.HasKey(L"unlock");
	BOOL list = parser.HasKey(L"locks");

	if (lock) {
		format = L"git.exe lfs lock \"%s\"";
	} else if (unlock) {
		format = L"git.exe lfs unlock \"%s\"";
	} else if (list) {
		
		CLocksDlg dlg;
		dlg.m_pathList = pathList;

		if (dlg.DoModal() == IDOK) {
			return TRUE;
		} else {
			return FALSE;
		}

	} else {
		return FALSE;
	}
	
	CString output;
	CString cmd;
	int nPath;
	for (nPath = 0; nPath < pathList.GetCount(); ++nPath)
	{
		cmd.Format(format, (LPCTSTR)pathList[nPath].GetGitPathString());
		if (g_Git.Run(cmd, &output, CP_UTF8))
		{
			if (CMessageBox::Show(hwndExplorer, output, L"TortoiseGit", 2, IDI_ERROR, CString(MAKEINTRESOURCE(IDS_IGNOREBUTTON)), CString(MAKEINTRESOURCE(IDS_ABORTBUTTON))) == 2)
				return FALSE;
		}
	}


	CShellUpdater::Instance().AddPathForUpdate(cmdLinePath);
	CShellUpdater::Instance().Flush();

	return TRUE;
}
