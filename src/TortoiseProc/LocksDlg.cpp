// TortoiseGit - a Windows shell extension for easy version control

// Copyright (C) 2009-2013, 2016-2017 - TortoiseGit
// Copyright (C) 2003-2008 - TortoiseSVN

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
#include "stdafx.h"
#include "TortoiseProc.h"
#include "MessageBox.h"
#include "LocksDlg.h"
#include "Git.h"
#include "PathUtils.h"
#include "registry.h"
#include "AppUtils.h"

#define REFRESHTIMER   100

IMPLEMENT_DYNAMIC(CLocksDlg, CResizableStandAloneDialog)
CLocksDlg::CLocksDlg(CWnd* pParent /*=nullptr*/)
	: CResizableStandAloneDialog(CLocksDlg::IDD, pParent)
	, m_bThreadRunning(FALSE)
	, m_bCancelled(false)
{
}

CLocksDlg::~CLocksDlg()
{
}

void CLocksDlg::DoDataExchange(CDataExchange* pDX)
{
	CResizableStandAloneDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOCKSLIST, m_LocksList);
}


BEGIN_MESSAGE_MAP(CLocksDlg, CResizableStandAloneDialog)
	ON_REGISTERED_MESSAGE(CGitStatusListCtrl::GITSLNM_NEEDSREFRESH, OnSVNStatusListCtrlNeedsRefresh)
	ON_REGISTERED_MESSAGE(CGitStatusListCtrl::GITSLNM_ADDFILE, OnFileDropped)
	ON_WM_TIMER()
END_MESSAGE_MAP()



BOOL CLocksDlg::OnInitDialog()
{
	CResizableStandAloneDialog::OnInitDialog();
	CAppUtils::MarkWindowAsUnpinnable(m_hWnd);

	m_LocksList.Init(GITSLC_COLEXT | GITSLC_COLLFSLOCK, L"LocksDlg", GITSLC_POPLFSLOCK | GITSLC_POPLFSUNLOCK);
	m_LocksList.SetConfirmButton((CButton*)GetDlgItem(IDOK));
	//m_LocksList.SetSelectButton(&m_SelectAll);
	//m_LocksList.SetCancelBool(&m_bCancelled);
	//m_LocksList.SetBackgroundImage(IDI_REVERT_BKG);
	m_LocksList.EnableFileDrop();

	CString sWindowTitle;
	GetWindowText(sWindowTitle);
	CAppUtils::SetWindowTitle(m_hWnd, g_Git.CombinePath(m_pathList.GetCommonRoot()), sWindowTitle);

	AdjustControlSize(IDC_SELECTALL);

	AddAnchor(IDC_LOCKSLIST, TOP_LEFT, BOTTOM_RIGHT);
	//AddAnchor(IDC_SELECTALL, BOTTOM_LEFT);
	//AddAnchor(IDC_UNVERSIONEDITEMS, BOTTOM_RIGHT);
	AddAnchor(IDOK, BOTTOM_RIGHT);
	//AddAnchor(IDCANCEL, BOTTOM_RIGHT);
	//AddAnchor(IDHELP, BOTTOM_RIGHT);
	if (hWndExplorer)
		CenterWindow(CWnd::FromHandle(hWndExplorer));
	EnableSaveRestore(L"LocksDlg");

	// first start a thread to obtain the file list with the status without 
	// blocking the dialog
	if (AfxBeginThread(LocksThreadEntry, this) == nullptr)
		CMessageBox::Show(this->m_hWnd, IDS_ERR_THREADSTARTFAILED, IDS_APPNAME, MB_OK | MB_ICONERROR);
	InterlockedExchange(&m_bThreadRunning, TRUE);

	return TRUE;
}

UINT CLocksDlg::LocksThreadEntry(LPVOID pVoid)
{
	return reinterpret_cast<CLocksDlg*>(pVoid)->LocksThread();
}

UINT CLocksDlg::LocksThread()
{
	// get the status of all selected file/folders recursively
	// and show the ones which can be reverted to the user
	// in a list control.
	DialogEnableWindow(IDOK, false);
	m_bCancelled = false;

	m_LocksList.StoreScrollPos();
	m_LocksList.Clear();

	g_Git.RefreshGitIndex();

	if (!m_LocksList.GetStatus(&m_pathList, false, false, false, false, true))
	{
		m_LocksList.SetEmptyString(m_LocksList.GetLastErrorMessage());
	}
	m_LocksList.Show(GITSLC_SHOWLFSLOCKS | GITSLC_SHOWINEXTERNALS, GITSLC_SHOWLFSLOCKS);
	/*
	if (m_LocksList.HasUnversionedItems())
	{
		if (DWORD(CRegStdDWORD(L"Software\\TortoiseGit\\UnversionedAsModified", FALSE)))
		{
			GetDlgItem(IDC_UNVERSIONEDITEMS)->ShowWindow(SW_SHOW);
		}
		else
			GetDlgItem(IDC_UNVERSIONEDITEMS)->ShowWindow(SW_HIDE);
	}
	else
		GetDlgItem(IDC_UNVERSIONEDITEMS)->ShowWindow(SW_HIDE);
	*/
	InterlockedExchange(&m_bThreadRunning, FALSE);
	RefreshCursor();

	return 0;
}

void CLocksDlg::OnOK()
{
	if (m_bThreadRunning)
		return;
	/*
	auto locker(m_RevertList.AcquireReadLock());
	// save only the files the user has selected into the temporary file
	m_bRecursive = TRUE;
	for (int i=0; i<m_RevertList.GetItemCount(); ++i)
	{
	if (!m_RevertList.GetCheck(i))
	m_bRecursive = FALSE;
	else
	{
	m_selectedPathList.AddPath(*m_RevertList.GetListEntry(i));
	#if 0
	CGitStatusListCtrl::FileEntry * entry = m_RevertList.GetListEntry(i);
	// add all selected entries to the list, except the ones with 'added'
	// status: we later *delete* all the entries in the list before
	// the actual revert is done (so the user has the reverted files
	// still in the trash bin to recover from), but it's not good to
	// delete added files because they're not restored by the revert.
	if (entry->status != svn_wc_status_added)
	m_selectedPathList.AddPath(entry->GetPath());
	// if an entry inside an external is selected, we can't revert
	// recursively anymore because the recursive revert stops at the
	// external boundaries.
	if (entry->IsInExternal())
	m_bRecursive = FALSE;
	#endif
	}
	}
	if (!m_bRecursive)
	m_RevertList.WriteCheckedNamesToPathList(m_pathList);
	m_selectedPathList.SortByPathname();
	*/

	CResizableStandAloneDialog::OnOK();
}

void CLocksDlg::OnCancel()
{
	m_bCancelled = true;
	if (m_bThreadRunning)
		return;

	CResizableStandAloneDialog::OnCancel();
}

void CLocksDlg::OnBnClickedSelectall()
{
	//UINT state = (m_SelectAll.GetState() & 0x0003);
	//if (state == BST_INDETERMINATE)
	//{
	//	// It is not at all useful to manually place the checkbox into the indeterminate state...
	//	// We will force this on to the unchecked state
	//	state = BST_UNCHECKED;
	//	m_SelectAll.SetCheck(state);
	//}
	//theApp.DoWaitCursor(1);
	//m_RevertList.SelectAll(state == BST_CHECKED);
	//theApp.DoWaitCursor(-1);
}

BOOL CLocksDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_RETURN:
		{
			if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
			{
				if (GetDlgItem(IDOK)->IsWindowEnabled())
					PostMessage(WM_COMMAND, IDOK);
				return TRUE;
			}
		}
		break;
		case VK_F5:
		{
			if (!m_bThreadRunning)
			{
				if (AfxBeginThread(LocksThreadEntry, this) == nullptr)
					CMessageBox::Show(this->m_hWnd, IDS_ERR_THREADSTARTFAILED, IDS_APPNAME, MB_OK | MB_ICONERROR);
				else
					InterlockedExchange(&m_bThreadRunning, TRUE);
			}
		}
		break;
		}
	}

	return CResizableStandAloneDialog::PreTranslateMessage(pMsg);
}

LRESULT CLocksDlg::OnSVNStatusListCtrlNeedsRefresh(WPARAM, LPARAM)
{
	if (AfxBeginThread(LocksThreadEntry, this) == nullptr)
		CMessageBox::Show(this->m_hWnd, IDS_ERR_THREADSTARTFAILED, IDS_APPNAME, MB_OK | MB_ICONERROR);
	return 0;
}

LRESULT CLocksDlg::OnFileDropped(WPARAM, LPARAM lParam)
{
	return 0;
}

void CLocksDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case REFRESHTIMER:
		if (m_bThreadRunning)
		{
			SetTimer(REFRESHTIMER, 200, nullptr);
			CTraceToOutputDebugString::Instance()(__FUNCTION__ ": Wait some more before refreshing\n");
		}
		else
		{
			KillTimer(REFRESHTIMER);
			CTraceToOutputDebugString::Instance()(__FUNCTION__ ": Refreshing after items dropped\n");
			OnSVNStatusListCtrlNeedsRefresh(0, 0);
		}
		break;
	}
	__super::OnTimer(nIDEvent);
}
